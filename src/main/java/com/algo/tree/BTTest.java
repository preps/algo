package com.algo.tree;

public class BTTest {

	public static void main(String[] args) {
		
		BinaryTree bt = new BinaryTree();
		bt.insert(1);
		bt.insert(6);
		bt.insert(4);
		bt.insert(7);
		bt.insert(8);
		bt.insert(3);
		
		bt.insert(10);
		bt.insert(14);
		bt.insert(13);
		//bt.display(bt.getRoot());
		//bt.bfs();
		//bt.dfs(bt.getRoot());
		//System.out.println("Is covered/uncovered tree sum equal : "+bt.findCoveredUncoveredNodesSum(bt.getRoot()));
		//System.out.println("Is tree can be divided into two by removing one leaf : "+bt.checkRec(bt.getRoot(), bt.count(bt.getRoot())));
		System.out.println("Max diff : "+bt.getMaxDiffParentChild(bt.getRoot()));
	}
}
