package com.algo.tree;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class BinaryTree {

	private Node root;

	public BinaryTree() {
		root = null;
	}

	public void insert(int value) {
		Node node = new Node(value);
		if (root == null) {
			root = node;
			return;
		}
		insert(root, node);
	}

	private void insert(Node root, Node node) {
		if (root.getData() < node.getData()) {
			if (root.getRight() == null)
				root.setRight(node);
			else
				insert(root.getRight(), node);
		} else if (root.getData() > node.getData()) {
			if (root.getLeft() == null)
				root.setLeft(node);
			else
				insert(root.getLeft(), node);
		}
		return;
	}

	public void display(Node root) {
		if (root != null) {
			display(root.getLeft());
			System.out.print("-> " + root.getData() + "\n");
			display(root.getRight());
		}
	}

	public void bfs() {
		Queue<Node> q = new LinkedBlockingQueue<Node>();
		Node item;
		if (root != null) {
			q.add(root);
			while (!q.isEmpty()) {
				item = (Node) q.remove();
				System.out.println("-->" + item);
				if (item != null) {
					if (item.getLeft() != null) {
						q.add(item.getLeft());
					}
					if (item.getRight() != null) {
						q.add(item.getRight());
					}
				}
			}
		}
	}

	public void dfs(Node root) {
		if(root==null)
			return;
		System.out.println("-->"+root.getData());
		if(root.getLeft()!=null)
			System.out.println("L-->");
			dfs(root.getLeft());
		
		if(root.getRight()!=null)
			System.out.println("R-->");
			dfs(root.getRight());
	}
	private Map<String,Integer>  getLeftView(Node node,Map<String,Integer> value, boolean isLeft){
		
		if(isLeft==true)
			value.put("covered", value.get("covered")+node.getData());
		else
			value.put("uncovered", value.get("uncovered")+node.getData());
		
		if(node.getLeft()!=null)
			getLeftView(node.getLeft(), value, true);
		else if(node.getRight()!=null)
			getLeftView(node.getRight(), value, true);
		
		 if(node.getRight()!=null)
		getLeftView(node.getRight(), value, false);	
		
		return value;
	}

	private Map<String,Integer>  getRightView(Node node,Map<String,Integer> value, boolean isRight){
		
		System.out.println("isRight : "+isRight+" , "+node.getData());
		
		if(isRight==true)
			value.put("covered", value.get("covered")+node.getData());
		else
			value.put("uncovered", value.get("uncovered")+node.getData());
		
		if(node.getRight()!=null)
			getRightView(node.getRight(), value, true);
		else if(node.getLeft()!=null)
			getRightView(node.getLeft(), value, true);
		
		if(node.getLeft()!=null)
		getRightView(node.getLeft(), value, false);	
		
		return value;
	}

	
	public boolean findCoveredUncoveredNodesSum(Node root) {
		
		Map<String,Integer> value = new HashMap<String, Integer>();
		value.put("covered", root.getData());
		value.put("uncovered", 0);
		
		if(root.getLeft()!=null)
		value = getLeftView(root.getLeft(), value, true);
		if(root.getRight()!=null)
		value = getRightView(root.getRight(), value, true);
		
		System.out.println("value : "+value);
		
		if(value.get("covered") == value.get("uncovered"))
			return true;
		else
			return false;
	}
	
	public int count(Node node){
		if(node==null)
			return 0;
/*		System.out.println("node : "+node.getData());
		if(node.getLeft()!=null)
		System.out.println("left : "+node.getLeft().getData());
		if(node.getRight()!=null)
		System.out.println("right : "+node.getRight().getData());*/
		return count(node.getLeft())+count(node.getRight())+1;
	}
	
	public boolean checkRec(Node root, int n)
	{
	    // Base cases
	    if (root == null)
	       return false;
	 
	    // Check for root
	    if (count(root) == n-count(root))
	        return true;
	 
	    // Check for rest of the nodes
	    return checkRec(root.getLeft(), n) || checkRec(root.getRight(), n);
	}
	 
	public int getMaxDiffParentChild(Node root){
	
		
		int leftResult = getMaxDiffBySide(root.getLeft());
		int rightResult = getMaxDiffBySide(root.getRight());
		
		if(leftResult>rightResult)
			return leftResult;
		else 
			return rightResult;
	}
	

	private int getMaxDiffBySide(Node root) {
		
		if(root==null)
			return 0;
		
		int max = 0;
		int min = 0;
		
		Queue<Node> q = new LinkedBlockingQueue<Node>();
		q.add(root);
		max = root.getData();
		min = root.getData();

		while(!q.isEmpty()){
			
			Node node = q.poll();
			
			if(node.getData()>max)
				max = node.getData();
			if(node.getData()<min)
				min = node.getData();
			
			if(node.getLeft()!=null)
				q.add(node.getLeft());
			if(node.getRight()!=null)
				q.add(node.getRight());
			
		}
		
		
		return (max-min);
		
	}

	public Node getRoot() {
		return root;
	}

	public void setRoot(Node root) {
		this.root = root;
	}
}
